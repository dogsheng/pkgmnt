import logging.config

import os
from flask import Flask, Blueprint
import settings
from api.endpoints.tracking import ns as tracking_namespace
from api.endpoints.issue import ns as issue_namespace
from api.restplus import api
from database import db
from task import task_apscheduler
from task import scheduler


app = Flask(__name__)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)


def add_job(job_id, func, args, seconds):
    """添加job"""
    log.info(f"添加job - {job_id}")
    scheduler.add_job(id=job_id, func=func, args=args, trigger='interval', seconds=seconds)


def configure_app(flask_app):
    flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP
    flask_app.config['RESTX_MASK_SWAGGER'] = False
    flask_app.config['SCHEDULER_EXECUTORS'] = {
        'default': {'type': 'threadpool', 'max_workers': 20}
    }


def initialize_app(flask_app):
    configure_app(flask_app)

    blueprint = Blueprint('', __name__, url_prefix='/')
    api.init_app(blueprint)
    api.add_namespace(tracking_namespace)
    api.add_namespace(issue_namespace)
    flask_app.register_blueprint(blueprint)

    db.init_app(flask_app)
    scheduler.init_app(flask_app)


def main():
    initialize_app(app)
    log.info('>>>>> Starting server at http://{}/ <<<<<'.format(app.config['SERVER_NAME']))

    with app.app_context():
        all_track = task_apscheduler.get_track_from_db()
        for track in all_track:
            add_job(job_id=str(track.id), func='task.task_apscheduler:single_upload_patch_to_gitee', args=(track,), seconds=3600)

    scheduler.start()
    app.run(debug=settings.FLASK_DEBUG)


if __name__ == "__main__":
    main()
