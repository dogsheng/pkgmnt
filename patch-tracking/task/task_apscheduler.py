from flask import current_app
from util.gitee_api import post_create_branch, post_upload_patch, post_create_issue, post_create_pull_request
from util.github_api import GitHubApi
from database.models import Tracking
from api.business import update_tracking, create_issue
from task import scheduler


def get_track_from_db():
    all_track = Tracking.query.filter_by(enabled=True)
    return all_track


def single_upload_patch_to_gitee(track):
    with scheduler.app.app_context():
        patch = single_get_scm_patch(track)
        issue = single_upload_patch(patch)
        single_create_issue_db(issue)


def single_get_scm_patch(track):
    '''
    遍历Tracking数据表, 获取enabled仓的patch文件，不同的仓有不同的获取方式
    :return:
    '''
    print('get scm patch')
    scm_dict = dict()
    github_api = GitHubApi()
    if track.enabled is True:
        scm_dict['scm_repo'] = track.scm_repo
        scm_dict['scm_branch'] = track.scm_branch
        scm_dict['scm_commit'] = track.scm_commit
        scm_dict['enabled'] = track.enabled
        scm_dict['repo'] = track.repo
        scm_dict['branch'] = track.branch

    status, result = github_api.get_latest_commit(scm_dict['scm_repo'], scm_dict['scm_branch'])
    print('get_latest_commit: ', status, result)

    if status == 'success':
        commitID = result
        if commitID != scm_dict['scm_commit']:
            scm_dict['last_scm_commit'] = commitID
            ret = github_api.get_patch(scm_dict['scm_repo'], scm_dict['scm_commit'], commitID)
            current_app.logger.info('get patch api ret ', ret)
            if ret['status'] == 'success':
                scm_dict['patch_content'] = ret['api_ret']
                data = {
                    'repo': scm_dict['repo'],
                    'branch': scm_dict['branch'],
                    'enabled': scm_dict['enabled'],
                    'scm_commit': commitID,
                    'scm_branch': scm_dict['scm_branch'],
                    'scm_repo': scm_dict['scm_repo']
                }
                update_tracking(data)
                return scm_dict
            else:
                current_app.logger.error('Error: github_api.get_patch failed. Repo: {}. Return val: {}'.format(scm_dict, ret['api_ret']))
        else:
            current_app.logger.info('Info: github_api.get_patch nothing change. Repo: {}'.format(scm_dict))
    else:
        current_app.logger.error('Error: github_api.get_latest_commit failed. Repo: {}. Return val: {}'.format(scm_dict, result))


def single_upload_patch(patch):
    '''
    创建临时分支，提交文件，创建PR和issue
    :return:
    '''
    print('upload patch')
    issue_dict = dict()
    if patch:
        repo = patch['repo']
        branch = patch['branch']
        issue_dict['repo'] = repo
        issue_dict['branch'] = branch
        new_branch = 'patch_tracking'
        patch_file_content = patch['patch_content']
        issue_body = 'patch file from commit {} to {}'.format(patch['scm_commit'], patch['last_scm_commit'])
        print(issue_body)

        post_create_branch(repo, branch, new_branch)
        post_upload_patch(repo, branch, patch['scm_commit'], patch['last_scm_commit'], str(patch_file_content))
        issue_num = post_create_issue(repo, issue_body)
        post_create_pull_request(repo, branch, new_branch, issue_num)

        issue_dict['issue'] = issue_num
        return issue_dict


def single_create_issue_db(issue):
    print('create issue')
    if issue:
        issue_num = issue['issue']
        print('issue_num: ', issue_num)
        tracking = Tracking.query.filter_by(repo=issue['repo'], branch=issue['branch']).first()
        tracking_id = tracking.id
        data = {
            'issue': issue_num,
            'tracking': tracking_id
        }
        current_app.logger.info('issue data', data)
        create_issue(data)
