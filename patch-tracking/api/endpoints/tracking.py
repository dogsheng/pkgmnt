import logging

from flask_restx import Resource
from api.serializers import tracking
from api.parsers import tracking_request
from api.restplus import api
from database.models import Tracking
from api.business import create_tracking
from flask import request

log = logging.getLogger(__name__)

ns = api.namespace('tracking', description='Operations related to tracking')


@ns.route('')
class TrackingCollection(Resource):
    @api.expect(tracking_request)
    @api.marshal_list_with(tracking)
    def get(self):
        """
        Returns list of tracking
        """
        trackings = Tracking.query.all()
        return trackings

    @api.expect(tracking)
    @api.response(201, 'Tracking successfully created.')
    @api.response(204, 'Tracking successfully updated.')
    def post(self):
        """
        Creates os update a tracking.
        """
        create_tracking(request.json)
        return None, 201
