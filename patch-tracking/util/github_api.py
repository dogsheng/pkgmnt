from flask import current_app
import logging.config
import requests
import time
import settings

log = logging.getLogger(__name__)


class GitHubApi:
    def __init__(self):
        token = 'token ' + settings.GITHUB_ACCESS_TOKEN
        self.headers = {
            'User-Agent': 'Mozilla/5.0',
            'Authorization': token,
            'Content-Type': 'application/json',
            'Connection': 'close',
            'method': 'GET',
            'Accept': 'application/json'
        }

    def api_request(self, url):
        current_app.logger.info("Connect url: {}".format(url))
        count = 15
        while count > 0:
            try:
                r = requests.get(url, headers=self.headers, verify=False)
                return r
            except:
                current_app.logger.info("Warning: Connect failed...sleep 5s...")
                time.sleep(5)
                count -= 1
                continue
        if count == 0:
            current_app.logger.error('Error: Fail to connnect to github: {}'.format(url))
            return 'connect error'


    def get_latest_commit(self, repo_url, branch):
        '''
        根据仓库名和分支获取最新commitID
        :param repo_url:
        :param branch:
        :return:
        '''
        api_url = 'https://api.github.com/repos'
        url = '/'.join([api_url, repo_url, 'branches', branch])
        ret = self.api_request(url)
        if ret != 'connect error':
            if ret.status_code == 200:
                latest_commit = ret.json()['commit']['sha']
                return 'success', latest_commit
            else:
                log.error('Error: {} failed. Return val: {}'.format(url, ret))
                return 'error', ret.json()
        else:
            return 'error', 'connect error'

    def get_patch(self, repo_url, scm_commit, last_commit):
        '''
        获取原commit和新commit之间的差异，在指定路径下创建patch文件
        :param repo_url:
        :param scm_commit:
        :param last_commit:
        :return:
        '''
        api_url = 'https://github.com'
        commit = scm_commit + '...' + last_commit + '.diff'
        ret_dict = dict()

        url = '/'.join([api_url, repo_url, 'compare', commit])
        ret = self.api_request(url)
        if ret != 'connect error':
            if ret.status_code == 200:
                patch_content = ret.text
                ret_dict['status'] = 'success'
                ret_dict['api_ret'] = patch_content
            else:
                current_app.logger.error('Error: {} failed. Return val: {}'.format(url, ret))
                ret_dict['status'] = 'error'
                ret_dict['api_ret'] = ret.text
        else:
            ret_dict['status'] = 'error'
            ret_dict['api_ret'] = 'fail to connect github by api.'

        return ret_dict
